from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^issue/new/$', IssueNew.as_view(), name='issue_new'),
    url(r'^issue/(?P<pk>[0-9]+)/edit/$', IssueEdit.as_view(), name='issue_edit'),
    url(r'^issue/(?P<pk>[0-9]+)/delete/$', IssueDelete.as_view(), name='issue_delete'),
    url(r'^register/$', RegisterFormView.as_view(), name='register'),
    url(r'^login/$', LoginFormView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),

]
