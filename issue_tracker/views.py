from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth.forms import AuthenticationForm
# from django.contrib.auth.forms import UserCreationFor
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.edit import FormView
from django.views.generic.list import ListView

from .forms import PostForm, RegisterForm
from .models import Issue, Person


class RegisterFormView(FormView):
    form_class = RegisterForm
    success_url = reverse_lazy('login')
    template_name = 'issue_tracker/register.html'

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "issue_tracker/login.html"
    success_url = "/"

    def form_valid(self, form):
        self.user = form.get_user()

        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect("/")

class IssueFiltered(ListView):
    model = Issue
    template_name = 'issue_tracker/index.html'


    def get_context_data(self, **kwargs):
        return super(IssueFiltered, self).get_context_data(**kwargs)


class IndexView(IssueFiltered):
    model = Issue
    template_name = 'issue_tracker/index.html'


class IssueNew(LoginRequiredMixin, CreateView):
    login_url = '/login/'
    form = PostForm
    model = Issue
    fields = form.Meta.fields
    template_name = 'issue_tracker/issue_edit.html'
    success_url = reverse_lazy('index')


    def form_valid(self, form):
        issue = form.save(commit=False)
        issue.issue_creator = Person.objects.get(person_name=self.request.user)
        issue.save()
        return super(IssueNew, self).form_valid(form)


class IssueEdit(LoginRequiredMixin, UpdateView):
    login_url = '/login/'
    model = Issue
    form = PostForm
    fields = form.Meta.fields
    template_name = 'issue_tracker/issue_edit.html'
    success_url = reverse_lazy('index')


class IssueDelete(LoginRequiredMixin, DeleteView):
    login_url = '/login/'
    model = Issue
    template_name = 'issue_tracker/issue_edit.html'
    success_url = reverse_lazy('index')