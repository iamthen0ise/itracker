from django.contrib.auth.models import User
from django.db import models
from django.conf import settings

# Create your models here.
class Issue(models.Model):
    issue_text = models.CharField(max_length=255)
    issue_description = models.TextField()
    issue_creator = models.ForeignKey('Person', related_name='issues_creator', default=0, )
    issue_assignee = models.ForeignKey('Person', related_name='issues_assignee', default=0, )
    issue_product = models.ForeignKey('Product', related_name='issues_product', default=0, )
    issue_kind = models.ForeignKey('IssueKind', related_name='issues_kind', default=0)
    issue_priority = models.ForeignKey('IssuePriority', related_name='issues_priority', default=0)
    issue_status = models.ForeignKey('IssueStatus', related_name='issues_status', default=0)
    
    def __str__(self):
        return self.issue_text


class Person(models.Model):
    person_name = models.OneToOneField(settings.AUTH_USER_MODEL, unique=True)
    person_dept = models.ForeignKey('Department', related_name='issues', default=0)

    def __str__(self):
        return self.person_name.get_full_name()


class Department(models.Model):
    department_name = models.TextField()
    department_type = models.TextField(blank=True)

    def __str__(self):
        return self.department_name


class IssueKind(models.Model):
    issue_kind_name = models.TextField()

    def __str__(self):
        return self.issue_kind_name


class IssuePriority(models.Model):
    issue_priority_name = models.TextField()
    issue_priority_weight = models.IntegerField()

    def __str__(self):
        return self.issue_priority_name


class Product(models.Model):
    product_name = models.TextField()
    product_version = models.FloatField()

    def __str__(self):
        return self.product_name


class IssueStatus(models.Model):
    status_name = models.TextField()

    def __str__(self):
        return self.status_name

