from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Issue, Person


class PostForm(forms.ModelForm):
    class Meta:
        model = Issue
        fields = ('issue_text', 'issue_description', 'issue_product',
                  'issue_kind', 'issue_priority', 'issue_assignee',
                  'issue_status')
        exclude = ('issue_creator',)


class RegisterForm(UserCreationForm):
    email = forms.EmailField(label="Email")
    first_name = forms.CharField(label="First Name")
    last_name = forms.CharField(label="Last Name")

    class Meta(UserCreationForm.Meta):
        fields = ("username", "first_name", "last_name", "email", )

    def save(self, commit=True):
        name = super(RegisterForm, self).save()
        return Person.objects.get_or_create(person_name=name)