from django.contrib import admin
# Register your models here.
from .models import *

# admin.site.register(Issue)
admin.site.register(IssueKind)
admin.site.register(IssuePriority)
admin.site.register(IssueStatus)
admin.site.register(Department)
admin.site.register(Product)
admin.site.register(Person)
